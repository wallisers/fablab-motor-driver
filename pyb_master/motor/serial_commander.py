"""
2019-03-16

Author: Sascha Walliser

Call movement functions by serial input
"""

import motor.motor_driver as md
import motor.input_validizer as input_validizer


def set_prefix(pre, val):
    """Checks if value is positive or negative by prefix"""
    input_validizer.validize_prefix(pre)
    if pre == 1:
        val = -val
    return val


def call_move(cmd):
    """Decodes cmd string and calls move"""
    spd = int(cmd[2:4], 16)
    drtn = int(cmd[4])
    dist = int(cmd[5:], 16)
    md.move(spd, drtn, dist)


def call_turn(cmd):
    """Decodes cmd string and calls turn"""
    spd = int(cmd[2:4], 16)
    deg = set_prefix(pre=int(cmd[4]), val=int(cmd[5:], 16))
    md.turn(spd, deg)


def call_circle(cmd):
    """Decodes cmd string and calls circle"""
    spd = int(cmd[2:4], 16)
    drtn = int(cmd[4])
    deg = set_prefix(pre=int(cmd[5]), val=int(cmd[6:], 16))
    md.circle(spd, drtn, deg)


def call_curve(cmd):
    """Decodes cmd string and calls curve"""
    spd = int(cmd[2:4], 16)
    drtn = int(cmd[4])
    deg = set_prefix(pre=int(cmd[5]), val=int(cmd[6:9], 16))
    rad = int(cmd[9:], 16)
    md.curve(spd, drtn, deg, rad)


def call_spin_wheel(cmd):
    """Decodes cmd string and calls spin_wheel"""
    spd = int(cmd[2:4], 16)
    drtn = int(cmd[4])
    dist = int(cmd[5:9], 16)
    mtr = int(cmd[9])
    md.spin_wheel(mtr, spd, drtn, dist)


def call_brake(cmd):
    """Decodes cmd string and calls brake"""
    spd = int(cmd[2:], 16)
    md.brake(spd)


def call_movement(cmd):
    """
    Analyses serial command string and
    passes it to function caller
    """
    cmd_type = cmd[0]
    input_validizer.validize_cmd_length(len(cmd), int(cmd[1], 16) + 2)
    switcher = {
        '1': call_move,
        '2': call_turn,
        '3': call_circle,
        '4': call_curve,
        '5': call_spin_wheel,
        'f': call_brake
        }
    switcher.get(cmd_type)(cmd)
