"""
2019-03-05

Author: Sascha Walliser, Pascal Dittrich

Error handling for motor functions
"""


from screen.oled1306 import display_status


def validize_cmd_length(given_len, calc_len):
    """Validizes cmd length from serial communication"""
    try:
        if given_len != calc_len:
            raise ValueError('Unexpected cmd string length')
    except ValueError:
        display_status('#serial_cmd', True)
        raise


def validize_prefix(pre):
    """Validizes prefix input for degree in serial communication"""
    try:
        if pre not in (0, 1):
            raise ValueError('Prefix value must be either 0 oder 1')
    except ValueError:
        display_status('#serial_cmd', True)
        raise


def validize_speed(func, speed):
    """Validizes input"""
    try:
        if speed not in range(101):
            raise ValueError('Speed must be between 0 and 100')
    except ValueError:
        display_status(func, True)
        raise


def validize_direction(func, direction):
    """Validizes input"""
    try:
        if direction not in (1, 2):
            raise ValueError('Direction must be either 1 or 2')
    except ValueError:
        display_status(func, True)
        raise


def validize_distance(func, distance, arg='Distance'):
    """Validizes input"""
    try:
        if distance < 0:
            raise ValueError('{} must be positive'.format(arg))
    except ValueError:
        display_status(func, True)
        raise


def validize_degree(func, degree):
    """Validizes input"""
    try:
        if degree not in range(-360, 361):
            raise ValueError('Degree must be between -360 and 360')
    except ValueError:
        display_status(func, True)
        raise


def validize_motor(func, motor):
    """Validizes input"""
    try:
        if motor not in (1, 2):
            raise ValueError('Motor must be either 1 or 2')
    except ValueError:
        display_status(func, True)
        raise
