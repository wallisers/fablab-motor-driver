"""
2019-01-20

Author: Sascha Walliser, Pascal Dittrich

Basic movement functions for robot
"""


import pyb
import utime

import motor.dist_calculator as dist_calculator
import motor.input_validizer as input_validizer
from screen.oled1306 import display_status


# Compensation factor for motors' production tolerance
comp = 0.85

# Pins right motor
rm_cw = pyb.Pin('Y2', pyb.Pin.OUT_PP)  # Clockwise rotation
rm_ccw = pyb.Pin('Y3', pyb.Pin.OUT_PP)  # Counterclockwise rotation
rm_pwm = pyb.Pin('Y1')  # PWM signal
# Pins left motor
lm_cw = pyb.Pin('Y5', pyb.Pin.OUT_PP)
lm_ccw = pyb.Pin('Y6', pyb.Pin.OUT_PP)
lm_pwm = pyb.Pin('Y4')
# Initialise
rm_cw.low()
rm_ccw.low()
lm_cw.low()
lm_ccw.low()

# Initialise PWM timers
max_freq = 8000  # Maximal frequency [Hz] for PWM timer
# Right motor
tim_r = pyb.Timer(8, freq=max_freq)
ch_r = tim_r.channel(1, pyb.Timer.PWM, pin=rm_pwm)
ch_r.pulse_width_percent(0)
# Left motor
tim_l = pyb.Timer(4, freq=max_freq)
ch_l = tim_l.channel(4, pyb.Timer.PWM, pin=lm_pwm)
ch_r.pulse_width_percent(0)

# Globals
speed_base = 612 / 100
smooth_delay = 10
smooth_delay_brk = 30


def reset_motors():
    """Resets motors to low"""
    rm_cw.low()
    rm_ccw.low()
    lm_cw.low()
    lm_ccw.low()


def accelerate(spd):
    """Smoothen acceleration at motors' start"""
    temp_spd = 0
    print('Accelerate')
    while spd > temp_spd:
        temp_spd += 1
        ch_r.pulse_width_percent(temp_spd)
        ch_l.pulse_width_percent(temp_spd // comp)
        utime.sleep_ms(smooth_delay)


def accelerate_async(spd_l, spd_r):
    """Smoothen acceleration for curve()"""
    temp_spd = 0
    print('Accelerate')
    if spd_l > spd_r:
        while spd_r > temp_spd:
            temp_spd += 1
            ch_r.pulse_width_percent(temp_spd)
            ch_l.pulse_width_percent(temp_spd // comp)
            utime.sleep_ms(smooth_delay)
        while spd_l > temp_spd:
            temp_spd += 1
            ch_l.pulse_width_percent(temp_spd // comp)
    elif spd_l < spd_r:
        while spd_l > temp_spd:
            temp_spd += 1
            ch_r.pulse_width_percent(temp_spd)
            ch_l.pulse_width_percent(temp_spd // comp)
            utime.sleep_ms(smooth_delay)
        while spd_r > temp_spd:
            temp_spd += 1
            ch_r.pulse_width_percent(temp_spd)


def brake(spd=0):
    """Stop motors smoothly"""
    while spd > 0:
        spd -= 1
        ch_r.pulse_width_percent(spd)
        ch_l.pulse_width_percent(spd // comp)
        utime.sleep_ms(smooth_delay_brk)
    rm_cw.high()
    rm_ccw.high()
    lm_cw.high()
    lm_ccw.high()
    display_status('brake')
    print('Stop')


def move(speed, direction, distance):
    """
    Moves forward/backward
    args: speed int(0, 100)[%], direction int(1, 2), distance int(>0)[mm]
    """
    name = 'main'
    reset_motors()
    input_validizer.validize_speed(name, speed)
    input_validizer.validize_direction(name, direction)
    input_validizer.validize_distance(name, distance)
    display_status(name, spd=speed, drtn=direction)
    print('Moving with s{}% in d{} for {}mm'.format(speed, direction, distance))
    if direction == 1:
        # Move forward
        rm_cw.high()
        lm_cw.high()
    elif direction == 2:
        # Move backward
        rm_ccw.high()
        lm_ccw.high()
    accelerate(speed)
    ch_r.pulse_width_percent(speed)
    ch_l.pulse_width_percent(speed // comp)
    utime.sleep_ms(dist_calculator.calc_distance_timer(speed, distance))
    brake(speed)


def turn(speed, degree):
    """
    Pinwheel left/right
    args: speed int(0, 100)[%], degree int(-360, 360)[°]
    """
    name = 'turn'
    reset_motors()
    input_validizer.validize_speed(name, speed)
    input_validizer.validize_degree(name, degree)
    display_status(name, spd=speed)
    print('Turning {}° with s{}%'.format(degree, speed))
    dist = dist_calculator.calc_distance(degree, 157.5)
    if degree > 0:
        # Turn left
        rm_cw.high()
        lm_ccw.high()
    elif degree < 0:
        # Turn right
        rm_ccw.high()
        lm_cw.high()
    accelerate(speed)
    ch_r.pulse_width_percent(speed)
    ch_l.pulse_width_percent(speed // comp)
    utime.sleep_ms(dist_calculator.calc_distance_timer(speed, dist))
    brake(speed)


def circle(speed, direction, degree):
    """
    Turn left/right. Inner wheel is stopped.
    args: speed int(0, 100)[%], direction int(1, 2),
        degree int(-360, 360)[°]
    """
    name = 'circle'
    reset_motors()
    input_validizer.validize_speed(name, speed)
    input_validizer.validize_direction(name, direction)
    input_validizer.validize_degree(name, degree)
    display_status(name, spd=speed, drtn=direction)
    print('Circle {}° with s{}% in d{}'.format(degree, speed, direction))
    dist = dist_calculator.calc_distance(degree, 315)
    if degree > 0:
        # Turn left
        if direction == 1:
            rm_cw.high()
            accelerate(speed)
            ch_r.pulse_width_percent(speed)
        elif direction == 2:
            lm_ccw.high()
            accelerate(speed)
            ch_l.pulse_width_percent(speed // comp)
        utime.sleep_ms(dist_calculator.calc_distance_timer(speed, dist))
    elif degree < 0:
        # Turn right
        if direction == 1:
            lm_cw.high()
            accelerate(speed)
            ch_l.pulse_width_percent(speed // comp)
        elif direction == 2:
            rm_ccw.high()
            accelerate(speed)
            ch_r.pulse_width_percent(speed)
        utime.sleep_ms(dist_calculator.calc_distance_timer(speed, dist))
    brake(speed)


def curve(speed, direction, degree, radius):
    """
    Curve left/right. Inner wheel is slower.
    Radius refers to the distance between the inner wheel and the center
        of a circle
    args: speed int(0, 100)[%], direction int(1, 2),
        degree int(-360, 360)[°], radius int(>0)[mm]
    """
    name = 'curve'
    reset_motors()
    input_validizer.validize_speed(name, speed)
    input_validizer.validize_direction(name, direction)
    input_validizer.validize_degree(name, degree)
    input_validizer.validize_distance(name, radius, arg='Radius')
    print('Curve {}° in d{}; rad{}mm'.format(degree, direction, radius))

    speed_final = speed_base * speed
    inner_distance = dist_calculator.calc_distance(degree, radius)
    distance = dist_calculator.calc_distance(degree, radius + 315)
    temp_dist_timer = distance / speed_final
    # Calculate speed for inner wheel
    inner_speed = inner_distance / temp_dist_timer
    inner_speed_final = int(inner_speed / 4.9)
    display_status(name, spd=speed, drtn=direction)
    print('Speed: {}%, {}%'.format(speed, inner_speed_final))

    if degree > 0:
        # Turn left
        if direction == 1:
            rm_cw.high()
            lm_cw.high()
        elif direction == 2:
            rm_ccw.high()
            lm_ccw.high()
        accelerate_async(inner_speed_final, speed)
        ch_r.pulse_width_percent(speed)
        ch_l.pulse_width_percent(inner_speed_final // comp)
        utime.sleep_ms(dist_calculator.calc_distance_timer(speed, distance))
    elif degree < 0:
        # Turn right
        if direction == 1:
            rm_cw.high()
            lm_cw.high()
        elif direction == 2:
            rm_ccw.high()
            lm_ccw.high()
        accelerate_async(speed, inner_speed_final)
        ch_r.pulse_width_percent(inner_speed_final)
        ch_l.pulse_width_percent(speed // comp)
        utime.sleep_ms(dist_calculator.calc_distance_timer(speed, distance))
    brake(speed)


def spin_wheel(motor, speed, direction, distance):
    """
    Controlling motors separately
    Good for balancing out motors' production variance
    args: motor str('l', 'r'), speed int(0, 100)[%],
        direction int(1, 2), distance int(>0)[mm]
    """
    name = 'spin_wheel'
    reset_motors()
    input_validizer.validize_motor(name, motor)
    input_validizer.validize_speed(name, speed)
    input_validizer.validize_direction(name, direction)
    input_validizer.validize_distance(name, distance)
    display_status(name, spd=speed, drtn=direction)
    print('Powerwing m{}: s{}%, d{}, {}mm'.format(motor, speed, direction, distance))
    if motor == 2:   # Right motor
        if direction == 1:
            mtr = rm_cw
        elif direction == 2:
            mtr = rm_ccw
        mtr_pwm = ch_r
    elif motor == 1:   # Left motor
        if direction == 1:
            mtr = lm_cw
        elif direction == 2:
            mtr = lm_ccw
        mtr_pwm = ch_l // comp
    mtr.high()
    accelerate(speed)
    mtr_pwm.pulse_width_percent(speed)
    utime.sleep_ms(dist_calculator.calc_distance_timer(speed, distance))
    brake(speed)
