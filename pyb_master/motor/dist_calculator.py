"""
2019-03-05

Author: Sascha Walliser, Pascal Dittrich

Calculate distance for basic movement
"""


from math import pi


# Globals
speed_base = 612/100
smooth_delay = 10
smooth_delay_brk = 30


def calc_distance_timer(speed, distance):
    """Calculates time needed to move a given distance"""
    smoothed_dist_acc, smoothed_dist_brk, time_acc = calc_smoothed_dist(speed)
    dist_clean = distance - smoothed_dist_acc - smoothed_dist_brk
    speed_final = speed_base * speed   # Calculate actual speed [mm/s]
    dist_timer_temp = int((dist_clean / speed_final) * 1000)
    dist_timer = dist_timer_temp + time_acc
    print('Duration: {}ms'.format(dist_timer))
    return dist_timer


def calc_distance(degree, radius):
    """Calculates distance for non straight movement"""
    distance = ((pi * radius) / 180) * degree
    if degree < 0:
        distance = -distance
    return distance


def calc_smoothed_dist(speed):
    """Calculates distance for acceleration/braking"""
    time_acc = speed * smooth_delay
    time_brk = speed * smooth_delay_brk
    speed_final = speed_base * speed   # Calculate actual speed [mm/s]
    # Acceleration
    acceleration_acc = speed_final / time_acc
    smoothed_dist_acc = ((acceleration_acc * time_acc**2) / 2) / 1000
    # Breaking
    # Note: Acceleration a ist negative when braking!
    # s = (at²/2) + vt; with a<0
    acceleration_brk = (speed_final / time_brk) * -1
    smoothed_dist_brk = (speed_final * time_brk +
                         (acceleration_brk * time_brk**2) / 2) / 1000
    return smoothed_dist_acc, smoothed_dist_brk, time_acc
