# FabLab Motor Driver

This is a motor controller for a two-wheeled robot. It is based on the __Marvin__ student project at Cooperative State University Heidenheim, Germany.


## Parts Used

+ PyBoard microcontroller 
+ Arduino Mega 2560 microcontroller
+ Pololu Dual VNH5019 motor shield
+ Adafruit SSD1306 oled display
+ 2 Faulhaber 3557 012 CR brushless DC motors
+ 2 Faulhaber IE2 -- 512 magnetic encoders (not implemented)
+ 2 Adafruit US-100 ultrasonic sensors (untested)
+ Adafruit VL6180X lidar sensor (untested)


## Robot Frame

+ Wheel diameter = 156 mm
+ Wheel perimeter = 490 mm
+ Outer wheel distance = 355 mm
+ Inner wheel distance = 280 mm
+ Mean wheel distance = 317 mm
+ Wheel width = 35 mm
+ Pinwheel diameter = 496 mm