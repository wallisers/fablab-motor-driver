"""
2019-01-20

Author: Sascha Walliser, Pascal Dittrich

FabLab project: Autonomous robot
"""


import machine
import pyb
# Option for verbose debugging:
# import micropython
# micropython.alloc_emergency_exception_buf(100)

from motor.serial_commander import call_movement
# from motor.motor_driver import brake
from screen.oled1306 import show_oled


version = '0.7'
show_oled([
    'MARVIN',
    '#' * 16,
    'Motor Driver',
    'v' + version,
    'S. Walliser &',
    'P. Dittrich',
    '#' * 16])

uart_usb = pyb.USB_VCP()
uart_wifi = pyb.UART(4, 57600)
# Initialise external interrupt for use of sensors on Arduino
# motor.motor_driver.brake is needed as callback function
# ATTENTION: Sensor functions (sensors.ino) are untested!
# ext_int = pyb.ExtInt(Pin('X22'), pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, brake)

while True:
    if uart_wifi.any() or uart_usb.any():
        serial_data = uart_wifi.readline()
        data_strp = serial_data.strip().decode('ascii')
        print(data_strp)
        call_movement(data_strp)
