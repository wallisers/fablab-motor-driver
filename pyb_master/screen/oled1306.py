"""
2019-03-06

Author: Sascha Walliser

Function for status display with OLED 1306
"""


import machine
import pyb
import screen.ssd1306 as ssd1306


i2c = machine.I2C(scl=pyb.Pin.board.Y9, sda=pyb.Pin.board.Y10)
oled = ssd1306.SSD1306_I2C(128, 64, i2c)


def show_oled(text):
    """
    Display text on OLED 1306
    args: list[7]
    """
    oled.fill(0)
    line_spacing = 9  # Line spacing [px]
    for i in range(7):
        oled.text(text[i], 0, i * line_spacing, 1)
        oled.show()


def display_status(func, error=False, spd=0, drtn=0):
    """Display status of motor driver with OLED screen"""
    status_text = ['MARVIN', '', '', '', '', '', '']
    if error is True:
        status_text[3:5] = 'Invalid input in', '{}'.format(func)
    else:
        if func == 'brake':
            status_text[3] = 'Ready'
        else:
            status_text = [
                'MARVIN',
                func,
                'Speed: {}%'.format(spd),
                'Direction: {}'.format(drtn),
                '', '', '']
    show_oled(status_text)
