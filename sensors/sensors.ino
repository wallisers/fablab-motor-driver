#include <Wire.h>
#include <HCSR04.h>
#include <Adafruit_VL6180X.h>


/*
 * 2019-03-17
 *
 * Author: Sascha Walliser, Pascal Dittrich
 * 
 * Analyse encoder ticks for each wheel
 * and control the sensors (two ultasonic and one infrared)
 *
 *
 * This code relies on external libraries for the sensors:
 *
 * - US-100 ultrasonic sensor
 *     enjoyneering/HCSR04 (GPLv3 license)
 *     git clone https://github.com/enjoyneering/HCSR04.git
 *
 * - VL6180X lidar
 *     adafruit/Adafruit_VL6180X (BSD license)
 *     git clone https://github.com/adafruit/Adafruit_VL6180X.git
 *
 *
 * ATTENTION: This code is untested!
 * All code for the use of the encoders is commented out
 */

// Initialise ultrasonic sensors
HCSR04 ultrasonicSensor_01(6, 7, 20, 300);
HCSR04 ultrasonicSensor_02(8, 9, 20, 300);
// Initialise ir sensor
Adafruit_VL6180X irSensor = Adafruit_VL6180X();

float distance = 0;
float lux = 0;

void setup()
{
  pinMode(22, OUTPUT);
  digitalWrite(22, HIGH);
  
  Serial.begin (9600);
  // Set trigger as output & echo pin as input for ultrasonic sensors
  ultrasonicSensor_01.begin();
  ultrasonicSensor_02.begin();
  // Wait for lidar
  Serial.println("Adafruit VL6180x");
  if (! irSensor.begin()) {
    Serial.println("Failed to find sensor");
    while (1);
  }
  Serial.println("Sensor found!");
}

void loop()
{
  digitalWrite(22, HIGH);
  
  /* 
   * Ultrasonic sensors
   */
  distance = ultrasonicSensor_01.getDistance();

  // (F()) saves string to flash & keeps dynamic memory free
  Serial.print(F("Sensor_01: "));
  if (distance != HCSR04_OUT_OF_RANGE) {
    Serial.println(distance);
  }
  else {
    Serial.println("Out of range");
  }
  if (distance <= 30.0) {
    // Interrupt to pyboard
    digitalWrite(22, LOW);
  }
  // Wait 50 ms or more, until echo from previous measurement disappears
  delay(50);
  // Pass 3 measurements through median filter, better result on moving obstacles
  distance = ultrasonicSensor_02.getMedianFilterDistance();
  
  Serial.print(F("Sensor_02: "));
  if (distance != HCSR04_OUT_OF_RANGE) {
    Serial.println(distance);
  }
  else {
    Serial.println("Out of range");
  }
  if (distance <= 30.0) {
    // Interrupt to pyboard
    digitalWrite(22, LOW);
  }
  // Serial refresh rate
  delay(200);
  
  
  /* 
   * Infrared sensor
   */
  float lux = irSensor.readLux(VL6180X_ALS_GAIN_5);

  Serial.print("Lux: ");
  Serial.println(lux);
  uint8_t range = irSensor.readRange();
  uint8_t status = irSensor.readRangeStatus();
  if (status == VL6180X_ERROR_NONE) {
    Serial.print("Range: ");
    Serial.println(range);
    if (range < 45 || range > 55) {
      //Interrupt to pyboard
      digitalWrite(22, LOW);
    }
  }
  // Print possible errors
  if  ((status >= VL6180X_ERROR_SYSERR_1) && (status <= VL6180X_ERROR_SYSERR_5)) {
    Serial.println("System error");
  }
  else if (status == VL6180X_ERROR_ECEFAIL) {
    Serial.println("ECE failure");
  }
  else if (status == VL6180X_ERROR_NOCONVERGE) {
    Serial.println("No convergence");
  }
  else if (status == VL6180X_ERROR_RANGEIGNORE) {
    Serial.println("Ignoring range");
  }
  else if (status == VL6180X_ERROR_SNR) {
    Serial.println("Signal/Noise error");
  }
  else if (status == VL6180X_ERROR_RAWUFLOW) {
    Serial.println("Raw reading underflow");
  }
  else if (status == VL6180X_ERROR_RAWOFLOW) {
    Serial.println("Raw reading overflow");
  }
  else if (status == VL6180X_ERROR_RANGEUFLOW) {
    Serial.println("Range reading underflow");
  }
  else if (status == VL6180X_ERROR_RANGEOFLOW) {
    Serial.println("Range reading overflow");
  }
  delay(50);
}



/*
 * ENCODERS
 */

/*
void setup()
{
    pinMode(LH_ENCODER_A, INPUT);
    pinMode(LH_ENCODER_B, INPUT);
    pinMode(RH_ENCODER_A, INPUT);
    pinMode(RH_ENCODER_B, INPUT);

    // Initialise interrupt handler
    attachInterrupt(0, leftEncoderEvent, CHANGE);
    attachInterrupt(1, rightEncoderEvent, CHANGE);

    Serial.begin(9600);	
}

void loop()
{
    Serial.print("Right count: ");
    Serial.println(rightCount);
    Serial.print("Left count: ");
    Serial.println(leftCount);
    Serial.println();
    delay(500);
}

void leftEncoderEvent()
{
    if (digitalRead(LH_ENCODER_A) == HIGH) {
        if (digitalRead(LH_ENCODER_B) == LOW) {
            leftCount++;
        } else {
            leftCount--;
        }
    } else {
        if (digitalRead(LH_ENCODER_B) == LOW) {
            leftCount--;
        } else {
            leftCount++;
        }
    }
}

void rightEncoderEvent()
{
    if (digitalRead(RH_ENCODER_A) == HIGH) {
        if (digitalRead(RH_ENCODER_B) == LOW) {
            rightCount++;
        } else {
            rightCount--;
        }
    } else {
        if (digitalRead(RH_ENCODER_B) == LOW) {
            rightCount--;
        } else {
            rightCount++;
        }
    }
}
*/
